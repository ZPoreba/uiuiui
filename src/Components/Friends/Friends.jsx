import * as React from 'react';
import { styles } from './Styles/Styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

class Friends extends React.Component {

  state = {
    names: ["Anna Kowalska", "Bartosz Nowak", "Sara Markowska", "Alicja Nowakowska",
      "Kamila Kasprzyk", "Damian Maleńczuk", "Adam Chorowski", "Krystian Kasprzyk",
      "Daniel Witwicki", "Maciej Chronowski", "Katarzyna Malik", "Tomasz Damiński"]
  }

  render() {
    const { classes, history } = this.props;
    const { names } = this.state;

    return <div>
      <CssBaseline />
      <main>
        <Typography component="h4" variant="h4" align="center" color="textPrimary" gutterBottom>
          Friends
         </Typography>
        <div className={`${classes.layout} ${classes.cardGrid}`}>
          <Grid container spacing={40}>
            {names.map((name, index) => (
              <Grid item key={index} sm={6} md={4} lg={3}>
                <Card className={classes.card} onClick={() => history.push('/profile/1')}>
                  <CardMedia
                    className={classes.cardMedia}
                    image={require(`../../Asset/friends/friend${index + 1}.jpg`)}
                    title="Profil Image"
                  />
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5" component="h2">
                      {name}
                    </Typography>
                  </CardContent>
                </Card>
              </Grid>
            ))}
          </Grid>
        </div>
      </main>
    </div>
  }
}

export default withStyles(styles)(Friends);