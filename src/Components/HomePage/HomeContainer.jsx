import * as React from 'react';
import { connect } from 'react-redux';
import { NotLogged } from './Components/NotLogged';
import Weather from './Components/Weather';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';

const Map = styled.img`
    height: 300px;
`;

const InfoWrapper = styled.div`
    margin-top: 20px;
`;

const card = {
    display: 'flex'
}

const Logo = styled.img`
    height: 200px;
    width: auto;
`;

class HomeContainer extends React.Component {
    state = {
        slopeInfos: [
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Kasprowy wierch',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Jaworzyna',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }
        ]
    }

    render() {
        const { isLoggIn, history } = this.props;
        const { slopeInfos } = this.state;

        return isLoggIn ? (
            <div>
                <Grid container spacing={16}>
                    <Grid item xs={12} md={10}>
                        <Card className={card}>
                            <Weather />
                        </Card>
                    </Grid>
                    <Grid item xs={12} md={2}>
                        <Card className={card}>
                            <Map src={require('../../Asset/mapa.jpg')} />
                        </Card>
                    </Grid>
                </Grid>
                <InfoWrapper>
                    {slopeInfos.map((info, index) => <Grid key={index} container spacing={16}>
                            <Grid item md={2} onClick={() => history.push('/slope')}>
                                <Card className={card}>
                                    <Logo src={info.img} />
                                </Card>
                            </Grid>
                            <Grid item md={10} onClick={() => history.push('/slope')}>
                                <Card className={card}>
                                    <Grid>
                                        <Card className={card}>
                                            <CardContent>
                                                <Typography component="h2" variant="h5">
                                                    {info.title}
                                                </Typography>
                                                <Typography variant="subtitle1" color="textSecondary">
                                                    {info.miniText}
                                                </Typography>
                                                <Typography variant="subtitle1" paragraph>
                                                    {info.fullText}
                                                </Typography>
                                            </CardContent>
                                        </Card>
                                    </Grid>
                                </Card>
                            </Grid>
                        </Grid>
                    )}
                </InfoWrapper>
            </div>
        ) : (
            <NotLogged />
        )
    }
}

const authentiacationState = state => ({
    isLoggIn: state.AuthenticationReducer.isLoggIn,
});

export default connect(
    authentiacationState
)(HomeContainer);