import React from 'react';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import LineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';

const data = [
  { name: 'Mon', 'Day max': 0, "Night min": -3 },
  { name: 'Tue', 'Day max': 1, "Night min": -2 },
  { name: 'Wed', 'Day max': -2, "Night min": -10 },
  { name: 'Thu', 'Day max': 0, "Night min": -3 },
  { name: 'Fri', 'Day max': -4, "Night min": -12 },
  { name: 'Sat', 'Day max': -1, "Night min": -4 },
  { name: 'Sun', 'Day max': 0, "Night min": -7 },
];

const Weather = () => (
    <ResponsiveContainer width="100%" height={300}>
    <LineChart data={data}>
      <XAxis dataKey="name" />
      <YAxis />
      <CartesianGrid vertical={false} strokeDasharray="3 3" />
      <Tooltip />
      <Legend />
      <Line type="monotone" dataKey='Day max' stroke="#82ca9d" />
      <Line type="monotone" dataKey="Night min" stroke="#8884d8" activeDot={{ r: 8 }} />
    </LineChart>
  </ResponsiveContainer>
)

export default Weather;