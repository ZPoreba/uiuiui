import * as React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    position: relative;
`;

const RespomsiveImg = styled.img`
    max-width: 100%;
    height: auto;
    width: auto;
`;

const Logo = styled.img`
    position: absolute;
    top: 40%;
    left: 45%;
    height: 150px;
`;

export const NotLogged = () => {
    return <Wrapper>
        <RespomsiveImg alt='login' src={require('../../../Asset/login.jpg')} />
        <Logo src={require('../../../Asset/favicon.ico')}/>
    </Wrapper>
}