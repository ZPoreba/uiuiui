import * as React from 'react';
import { styles } from './Styles/Styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LocationOn from '@material-ui/icons/LocationOn';
import Favorite from '@material-ui/icons/Favorite';
import Message from '@material-ui/icons/Message';
import Home from '@material-ui/icons/Home';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import AddPhotoAlternate from '@material-ui/icons/AddPhotoAlternate';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Switch from '@material-ui/core/Switch';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

class Profile extends React.Component {
  state = {
    friendsNames: ["Anna Kowalska", "Bartosz Nowak", "Sara Markowska", "Alicja Nowakowska",
      "Kamila Kasprzyk", "Damian Maleńczuk", "Adam Chorowski", "Krystian Kasprzyk",
      "Daniel Witwicki", "Maciej Chronowski", "Katarzyna Malik", "Tomasz Damiński"],
    isMyProfile: false,
    editMode: false,
    openDialog: false,
    isProfilePhoto: true,
    name: "Anna Kowalska",
    address: "Some Place 23/3 23-123",
    date: "2001-03-03",
    resort: "Zakopane",
    tileData: [{
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 2,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }, {
      title: 'Image',
      author: 'author',
      cols: 1,
    }
    ],
    openMessageDialog: false
  }

  onSwitchChange = () => this.setState(currentState => ({
    isProfilePhoto: !currentState.isProfilePhoto
  }));

  componentDidMount() {
    this.setState({
      isMyProfile: this.props.match.params.id === undefined
    })
  }

  onEditChange = () => this.setState(currentState => ({
    editMode: !currentState.editMode,
  }));

  onInputChange = (event) => this.setState({
    [event.target.name]: event.target.value
  });

  changeProfile = () => {
    this.props.history.push('/profile/1');
    this.setState({ isMyProfile: false })
  }

  onCancel = () => this.setState(currentState => ({
    editMode: !currentState.editMode,
    name: "Anna Kowalska",
    address: "Some Place 23/3 23-123",
    date: "2001-03-03",
    resort: "Zakopane"
  }));

  render() {
    const { classes } = this.props;
    const { friendsNames, isMyProfile, editMode, openDialog, isProfilePhoto, name, address, date, resort, tileData, openMessageDialog } = this.state;

    return <div>
      <CssBaseline />
      <main>
        {isMyProfile && <Button onClick={this.onEditChange}>{editMode ? 'Save' : 'Edit'}</Button>}
        {editMode && isMyProfile && <Button onClick={this.onCancel}>Cancel</Button>}
        {editMode && <Button onClick={() => this.setState({ openDialog: true })}><AddPhotoAlternate /></Button>}
        <div className={`${classes.cardGrid}`}>
          <Grid container spacing={6}>
            <Grid item xs={12} md={12} className={classes.gridStyle}>
              <img src={require('../../Asset/profile_background.jpg')} alt="..." className={classes.bgImage} />
              <Avatar alt="Remy Sharp" src={require('../../Asset/friends/friend4.jpg')} align="center" className={classes.avatar} />
              {editMode ? <TextField id="standard-name" label="Name" name="name" value={name} className={classes.textField} onChange={this.onInputChange} margin="normal" /> :
                <Typography component="h4" variant="h4" color="textPrimary" className={classes.name} gutterBottom>
                  {name}
                  {!isMyProfile && <Button onClick={() => this.setState({ openMessageDialog: true })}><Message /></Button>}
                </Typography>
              }
            </Grid>
            <Grid item xs={12} md={12}>
              {!editMode && <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.heading}>Friends</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Grid container spacing={40} className={classes.layout}>
                    {friendsNames.map((name, index) => (
                      <Grid item key={index} sm={6} md={4} lg={3}>
                        <Card className={classes.card} onClick={this.changeProfile}>
                          <CardMedia
                            className={classes.cardMedia}
                            image={require(`../../Asset/friends/friend${index + 1}.jpg`)}
                            title="Profil Image"
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="h6" component="h6">
                              {name}
                            </Typography>
                          </CardContent>
                        </Card>
                      </Grid>
                    ))}
                  </Grid>
                </ExpansionPanelDetails>
              </ExpansionPanel>}
            </Grid>
            <Grid item xs={12} md={12} className={classes.tab}>
              <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.heading}>Personal information</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <List className={classes.root}>
                    <ListItem>
                      <Avatar>
                        <LocationOn />
                      </Avatar>
                      {!editMode ? <ListItemText primary="Addres" secondary={address} /> : <TextField id="standard-name" label="Addres" name="address" onChange={this.onInputChange} value={address} className={classes.textField} margin="normal" />}
                    </ListItem>
                    <ListItem>
                      <Avatar>
                        <Favorite />
                      </Avatar>
                      {!editMode ? <ListItemText primary="Birthday" secondary={date} /> : <TextField id="date" label="Birthday" type="date" name="date" onChange={this.onInputChange} value={date} defaultValue="2017-05-24" className={classes.textField} />}
                    </ListItem>
                    <ListItem>
                      <Avatar>
                        <Home />
                      </Avatar>
                      {!editMode ? <ListItemText primary="Home resort" secondary={resort} /> : <TextField id="standard-name" label="Home resort" name="resort" onChange={this.onInputChange} value={resort} className={classes.textField} margin="normal" />}
                    </ListItem>
                  </List>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Grid>
            {!editMode && <Grid item xs={12} md={12} className={classes.tab}>
              <ExpansionPanel>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.heading}>Gallery</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <div className={classes.root}>
                    <GridList cellHeight={160} className={classes.gridList} cols={3}>
                      {tileData.map((tile, index) => (
                        <GridListTile key={index} cols={tile.cols || 1}>
                          <img src={require(`../../Asset/profileGallery/m${index}.jpg`)} alt={tile.title} />
                        </GridListTile>
                      ))}
                    </GridList>
                  </div>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Grid>}
          </Grid>

          <Dialog open={openDialog} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
            <DialogTitle id="alert-dialog-title">{isProfilePhoto ? "Profile" : "Background"} photo <Switch onClick={this.onSwitchChange} color="primary" value={isProfilePhoto} /></DialogTitle>
            <DialogActions>
              <TextField type="file" accept="image/*" />
              <Button onClick={() => this.setState({ openDialog: false })}>
                Cancel
              </Button>
            </DialogActions>
          </Dialog>

          <Dialog open={openMessageDialog} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
            <TextField
              className={classes.messageInput}
              id="outlined-multiline-flexible"
              label="Message"
              multiline
              rowsMax="10"
              margin="normal"
              variant="outlined"
            />
            <DialogActions>
              <Button onClick={() => this.setState({ openMessageDialog: false })}>
                Send
                    </Button>
              <Button onClick={() => this.setState({ openMessageDialog: false })}>
                Cancel
                    </Button>
            </DialogActions>
          </Dialog>
        </div>
      </main>
    </div>
  }
}

export default withStyles(styles)(Profile);
