export const styles = theme => ({
  avatar: {
    marginTop: 200,
    marginLeft: 100,
    width: 220,
    height: 220,
    border: '7px solid #F0F8FF'
  },
  bgImage: {
    position: 'absolute',
    height: 300,
    "max-width": '100%',
    "width": '120%'
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(1100 + theme.spacing.unit * 3 * 2)]: {
      width: 650,
      marginLeft: 'auto',
      marginRight: 'auto'
    },
  },
  cardGrid: {
    padding: `${theme.spacing.unit * 2}px 0`
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%'
  },
  cardContent: {
    flexGrow: 1
  },
  gridStyle: {
    position: 'relative'
  },
  name: {
    marginLeft: 100
  },
  info: {
    marginLeft: "10px"
  },
  root: {
    flexGrow: 1,
  },
  header: {
    display: 'flex',
    alignItems: 'center',
    height: 50,
    paddingLeft: theme.spacing.unit * 4,
    backgroundColor: theme.palette.background.default,
  },
  img: {
    height: 255,
    maxWidth: 400,
    overflow: 'hidden',
    display: 'block',
    width: '100%',
  },
  tab: {
    marginTop: '10px'
  },
  gallery: {
    height: 700,
    "width": 'auto'
  },
  textField: {
    marginLeft: 10
  },
  messageInput: {
    width: '500px',
    padding: '10px'
  }
});
