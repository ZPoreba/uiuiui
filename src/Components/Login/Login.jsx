import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import LockIcon from '@material-ui/icons/LockOutlined';
import SocialLogin from '../Common/SocialLogin';
import { styles } from './Styles/Styles';
import { connect } from 'react-redux';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Tooltip from '@material-ui/core/Tooltip';
import { Redirect } from 'react-router';
import { login } from '../../Redux/Actions/Actions';
import { bindActionCreators } from 'redux';

class Login extends React.Component {
    state = {
        validation: {
            isPasswordValid: true,
            isEmailValid: true
        },
        email: '',
        password: '',
        message: '',
        showMessage: false
    }

    onInputChange = (event) => this.setState({
        [event.target.name]: event.target.value
    });

    login = (event) => {
        event.preventDefault();
        const emailRegex = new RegExp("^[^@]+@[^@]+.[^@]+$");
        const passwordRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");

        if (this.state.email.trim() === '') {
            this.setState({
                showMessage: true,
                message: 'Email can not be empty!'
            });
        } else if (this.state.password.trim() === ''){
            this.setState({
                showMessage: true,
                message: 'Password can not be empty!'
            });
        }
        else {
            this.setState(currentState => ({
                validation: {
                    isPasswordValid: passwordRegex.test(String(currentState.password)),
                    isEmailValid: emailRegex.test(String(currentState.email).toLowerCase()),
                },
                showMessage: !passwordRegex.test(String(currentState.password)) || !emailRegex.test(String(currentState.email).toLowerCase()),
                message: `Invalid ${passwordRegex.test(String(currentState.password)) ? '' : 'password'} ${emailRegex.test(String(currentState.email).toLowerCase()) ? '' : 'email'}`
            }), () => {
                const { validation } = this.state;

                if (validation.isPasswordValid && validation.isEmailValid) {
                    const { password, email } = this.state;
                    if (password === this.props.password && email === this.props.email) {
                        this.props.login();
                        this.props.history.push('/');
                    } else {
                        this.setState({
                            showMessage: true,
                            message: 'Incorrect email or password'
                        });
                    }
                }
            });
        }
    }

    closeButton = (
        <Button color="secondary" size="small" onClick={() => this.setState({ showMessage: false })}>
            CLOSE
        </Button>
    );

    render() {
        const { classes, isLoggIn } = this.props;
        const { password, email, validation, message, showMessage } = this.state;

        return isLoggIn ? (
            <Redirect to='/' />
        ) : (<main className={classes.main}>
            <CssBaseline />
            {showMessage && <SnackbarContent className={classes.snackbar} message={message} action={this.closeButton} />}
            <Paper className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={classes.form}>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="email">Email Address</InputLabel>
                        <Input required id="email" name="email" autoComplete="email" error={!validation.isEmailValid} onChange={this.onInputChange} autoFocus value={email} />
                    </FormControl>
                    <FormControl margin="normal" required fullWidth>
                        <InputLabel htmlFor="password">Password</InputLabel>
                        <Tooltip title="Password should be at leat 8 character long and contain one special character and one big letter" placement="top-start">
                            <Input required name="password" type="password" id="password" error={!validation.isPasswordValid} onChange={this.onInputChange} autoComplete="current-password" value={password} />
                        </Tooltip>
                    </FormControl>
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" />}
                        label="Remember me"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={this.login}
                    >
                        Sign in
                    </Button>
                    <SocialLogin />
                </form>
            </Paper>
        </main>
            )
    }
}

const loginActionMapper = dispatch => {
    return bindActionCreators({ login }, dispatch);
};

const isLoggedIn = state => ({
    isLoggIn: state.AuthenticationReducer.isLoggIn,
    email: state.AuthenticationReducer.email,
    password: state.AuthenticationReducer.password
});

export default connect(
    isLoggedIn,
    loginActionMapper
)(
    withStyles(
        styles
    )(Login)
);