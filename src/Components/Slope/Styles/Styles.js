export const styles = {
    root: {
        marginTop: '10px',
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    card: {
        display: 'flex'
    },
    title: {
        marginBottom: '10px'
    },
    inline: {
        display: 'inline',
    },
    opinios: {
        width: '100%',
        maxWidth: 1500,
    },
    tab: {
      marginTop: '10px'
    },
    messageInput: {
      width: '500px',
      padding: '10px'
    }
};