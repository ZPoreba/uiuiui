import * as React from 'react';
import Weather from '../HomePage/Components/Weather';
import Grid from '@material-ui/core/Grid';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import { SnackbarContent } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './Styles/Styles';
import Typography from '@material-ui/core/Typography';
import Message from '@material-ui/icons/Message';
import Map from '@material-ui/icons/Map';
import Button from '@material-ui/core/Button';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import Badge from '@material-ui/core/Badge';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';


const MainPhoto = styled.img`
    height: 300px;
`;

const card = {
    display: 'flex'
}

const InfoWrapper = styled.div`
    margin-top: 20px;
`;

class Slope extends React.Component {
    state = {
        liveInformation: ["Lorem ipsum dyrdum...", "Lorem ipsum dyrdum...", "Lorem ipsum dyrdum...", "Lorem ipsum dyrdum...", "Lorem ipsum dyrdum..."],
        cards: [
            {
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }, {
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
            }
        ],
        opinions: [{
            rating: 4,
            title: "Thats title..",
            opinion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }, {
            rating: 5,
            title: "Thats title..",
            opinion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }, {
            rating: 2,
            title: "Thats title..",
            opinion: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
        }],
        openDialog: false
    }

    render() {
        const { liveInformation, cards, opinions, openDialog } = this.state;
        const { classes } = this.props;

        return <div>
            <Grid container spacing={40}>
                <Grid item xs={12} md={11}>
                    <Card className={card}>
                        <Typography variant="h5" component="h3">
                            Kasprowy Wierch - PKL
                            </Typography>
                        <Typography component="p">
                            Zakopane
                        </Typography>
                    </Card>
                </Grid>
                <Grid item xs={12} md={1}>
                    <Card className={card}>
                        <Button onClick={() => this.setState({ openDialog: true })}>
                            <Message />
                        </Button>
                        <Button>
                            <Map />
                        </Button>
                    </Card>
                </Grid>
            </Grid>
            <Grid container spacing={40}>
                <Grid item xs={12} md={4}>
                    <Card className={card}>
                        <MainPhoto src={require('../../Asset/kasprowy.jpg')} />
                    </Card>
                </Grid>
                <Grid item xs={12} md={5}>
                    <Card className={card}>
                        <Weather />
                    </Card>
                </Grid>
                <Grid item xs={12} md={3}>
                    <Card className={card}>
                        {
                            liveInformation.map((item, index) => <SnackbarContent key={index} message={item} />)
                        }
                        <Paper className={classes.root} elevation={2}>
                            <InputBase className={classes.input} placeholder="Send live message" />
                            <IconButton color="primary" className={classes.iconButton} aria-label="Directions">
                                <DirectionsIcon />
                            </IconButton>
                        </Paper>
                    </Card>
                </Grid>
            </Grid>
            {cards.map((item, index) => <InfoWrapper key={index}>
                <Grid item xs={12} md={12}>
                    <Card className={card}>
                        <Grid>
                            <Card className={card}>
                                <CardContent>
                                    <Typography component="h2" variant="h5">
                                        {item.title}
                                    </Typography>
                                    <Typography variant="subtitle1" color="textSecondary">
                                        {item.miniText}
                                    </Typography>
                                    <Typography variant="subtitle1" paragraph>
                                        {item.fullText}
                                    </Typography>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Card>
                </Grid>
            </InfoWrapper>
            )}

            <Grid item xs={12} md={12} className={classes.tab}>
                <ExpansionPanel>
                    <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                        <Badge color="secondary" badgeContent={3} >
                            <Typography>Opinions</Typography>
                        </Badge>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <List className={classes.opinios}>
                            {opinions.map((opinion, index) => <div key={index}>
                                <ListItem alignItems="flex-start">
                                    <ListItemAvatar>
                                        <Badge color="secondary" badgeContent={opinion.rating} >
                                            <Avatar alt="Remy Sharp" src={require(`../../Asset/friends/friend${index + 1}.jpg`)} />
                                        </Badge>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary={opinion.title}
                                        secondary={
                                            <React.Fragment>
                                                {opinion.opinion}
                                            </React.Fragment>
                                        }
                                    />
                                </ListItem>
                                <Divider />
                            </div>
                            )}
                        </List>
                    </ExpansionPanelDetails>
                </ExpansionPanel>
            </Grid>

            <Dialog open={openDialog} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                <TextField
                    className={classes.messageInput}
                    id="outlined-multiline-flexible"
                    label="Message"
                    multiline
                    rowsMax="10"
                    margin="normal"
                    variant="outlined"
                />
                <DialogActions>
                    <Button onClick={() => this.setState({ openDialog: false })}>
                        Send
                    </Button>
                    <Button onClick={() => this.setState({ openDialog: false })}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    }
}

export default withStyles(styles)(Slope);