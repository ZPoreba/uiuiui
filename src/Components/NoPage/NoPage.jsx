import * as React from 'react';
import { Background, WrapperDiv } from './Components/Components';

const NoPage = () => (
    <WrapperDiv>
        <Background src={require('../../Asset/NoPage.gif')} />
    </WrapperDiv>
);

export default NoPage;