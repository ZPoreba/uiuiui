import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const WrapperDiv = styled.div`
    min-height: 100%;
    min-width: 1024px;
    width: 100%;
    position: fixed;
`;

export const Background = styled.img`
    min-height: 100%;
    min-width: 1024px;
    width: 100%;
    height: auto;
    position: fixed;
    top: 0;
    left: 0;
`;

export const GoHome = styled(Link)`
    position: absolute;
    top: 80%;
    left: 46%;
`;