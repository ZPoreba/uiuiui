import * as React from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Badge from '@material-ui/core/Badge';

const Logo = styled.img`
    height: 145px;
    width: auto;
`;

const card = {
    display: 'flex'
}

class SlopeSearch extends React.Component {
    state = {
        slopeInfos: [
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            },
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            },
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            },
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            },
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            },
            {
                img: require("../../Asset/icon-jaworzyna.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }, {
                img: require("../../Asset/icon-mosorny-gron.png"),
                title: 'Lorem ipsum',
                miniText: 'Lorem ipsum',
                fullText: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
            }
        ]
    }

    render() {
        const { slopeInfos } = this.state;
        const { history } = this.props;

        return <div>
            {slopeInfos.map((info, index) => <Grid key={index} container spacing={40}>
                <Grid item xs={12} md={2}>
                    <Badge color="secondary" badgeContent={index} >
                        <Card className={card}>
                            <Logo src={info.img} />
                        </Card>
                    </Badge>
                </Grid>
                <Grid item xs={12} md={10}>
                    <Card className={card}>
                        <Grid>
                            <Card className={card}>
                                <CardContent>
                                    <Typography component="h2" variant="h5">
                                        {info.title}
                                    </Typography>
                                    <Typography variant="subtitle1" color="textSecondary">
                                        {info.miniText}
                                    </Typography>
                                    <Typography variant="subtitle1" paragraph>
                                        {info.fullText}
                                    </Typography>
                                    <Button variant="outlined" color="primary" onClick={() => history.push('/slope')}>
                                        Slope side
                                    </Button>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Card>
                </Grid>
            </Grid>
            )}
        </div>
    }
}

export default SlopeSearch;