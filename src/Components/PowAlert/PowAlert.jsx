import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import { styles } from './Styles/Styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import LocalPhone from '@material-ui/icons/LocalPhone';
import TextField from '@material-ui/core/TextField';
import Drafts from '@material-ui/icons/Drafts';
import IconButton from '@material-ui/core/IconButton';
import Checkbox from '@material-ui/core/Checkbox';
import AddCircle from '@material-ui/icons/AddCircle';
import RemoveCircle from '@material-ui/icons/RemoveCircle';
import SaveIcon from '@material-ui/icons/Save';
import Button from '@material-ui/core/Button';

class PowAlert extends React.Component {

    state = {
        checked: [1],
        globalSlopes: ["Lorem ipsum 1", "Lorem ipsum 2", "Lorem ipsum 3", "Lorem ipsum 4", "Lorem ipsum 5", "Lorem ipsum 6"],
        choosenSlopes: ["Lorem ipsum 7", "Lorem ipsum 8", "Lorem ipsum 9"],
        scroll: 'paper',
        checkBox: ["Fresh snow", "Daily weather condition", "Concrete ski lift status", "Deegree day", "Slopes events"]
    };

    handleToggle = value => () => {
        const { checked } = this.state;
        const currentIndex = checked.indexOf(value);
        const newChecked = [...checked];

        if (currentIndex === -1) {
            newChecked.push(value);
        } else {
            newChecked.splice(currentIndex, 1);
        }

        this.setState({
            checked: newChecked,
        });
    };

    addSlope = (slope) => {
        const item = this.state.globalSlopes.find(x => x === slope);
        this.setState(currentState => ({
            globalSlopes: [...currentState.globalSlopes.filter(x => x !== slope)],
            choosenSlopes: [...currentState.choosenSlopes, item]
        }));
    }

    removeSlope = (slope) => {
        const item = this.state.choosenSlopes.find(x => x === slope);
        this.setState(currentState => ({
            choosenSlopes: [...currentState.choosenSlopes.filter(x => x !== slope)],
            globalSlopes: [...currentState.globalSlopes, item]
        }));
    }

    render() {
        const { classes } = this.props;
        const { globalSlopes, choosenSlopes, checkBox } = this.state;

        return (
            <React.Fragment>
                <CssBaseline />
                <main className={classes.layout}>
                    <div className={classes.heroContent}>
                        <Typography component="h1" variant="h2" align="center" color="textPrimary" gutterBottom>
                            Pow Alerts
                        </Typography>
                    </div>
                    <Grid container spacing={40} alignItems="flex-end">
                        <Grid item xs={12} md={4}>
                            <Card>
                                <CardHeader
                                    title="All Slopes"
                                    titleTypographyProps={{ align: 'center' }}
                                    subheaderTypographyProps={{ align: 'center' }}
                                    className={classes.cardHeader}
                                />
                                <CardContent>
                                    <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Search…"
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                        />
                                    </div>
                                    <div className={classes.cardPricing}>
                                        <List dense className={classes.listStyle}>
                                            {globalSlopes.map((slope) => (
                                                <ListItem key={slope} role={undefined} dense button>
                                                    <ListItemAvatar>
                                                        <Avatar
                                                            alt={slope}
                                                            src={require(`../../Asset/skiProfile.jpg`)} />
                                                    </ListItemAvatar>
                                                    <ListItemText primary={slope} />
                                                    <ListItemSecondaryAction>
                                                        <IconButton aria-label="Comments" onClick={() => this.addSlope(slope)}>
                                                            <AddCircle />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            ))}
                                        </List>
                                    </div>
                                </CardContent>
                            </Card>
                        </Grid>

                        <Grid item xs={12} md={4}>
                            <Card>
                                <CardHeader
                                    title="Subscribed Slopes"
                                    titleTypographyProps={{ align: 'center' }}
                                    subheaderTypographyProps={{ align: 'center' }}
                                    className={classes.cardHeader}
                                />
                                <CardContent>
                                    <div className={classes.search}>
                                        <div className={classes.searchIcon}>
                                            <SearchIcon />
                                        </div>
                                        <InputBase
                                            placeholder="Search…"
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                        />
                                    </div>
                                    <div className={classes.cardPricing2}>
                                        <List dense className={classes.listStyle}>
                                            {choosenSlopes.map((value) => (
                                                <ListItem key={value} role={undefined} dense button >
                                                    <ListItemAvatar>
                                                        <Avatar
                                                            alt={`Avatar n°${value + 1}`}
                                                            src={require(`../../Asset/skiProfile.jpg`)} />
                                                    </ListItemAvatar>
                                                    <ListItemText primary={value} />
                                                    <ListItemSecondaryAction>
                                                        <IconButton aria-label="Comments" onClick={() => this.removeSlope(value)}>
                                                            <RemoveCircle />
                                                        </IconButton>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            ))}
                                        </List>
                                    </div>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={12} md={4}>
                            <Button
                                variant="contained"
                                color="primary"
                            >
                                <SaveIcon />
                            </Button>
                            <List className={classes.root}>
                                <ListItem>
                                    <Avatar>
                                        <LocalPhone />
                                    </Avatar>
                                    <TextField id="standard-name" label="Phone" className={classes.textField} margin="normal" />
                                </ListItem>
                                <ListItem>
                                    <Avatar>
                                        <Drafts />
                                    </Avatar>
                                    <TextField id="standard-name" label="Email" className={classes.textField} margin="normal" />
                                </ListItem>
                            </List>
                            <Card>
                                <CardContent>
                                    <div className={classes.cardPricing3}>
                                        <List dense className={classes.listStyle}>
                                            {checkBox.map((item, index) => <ListItem key={index} role={undefined} dense button >
                                                <ListItemText primary={item} />
                                                <Checkbox
                                                    tabIndex={-1}
                                                    disableRipple
                                                />
                                            </ListItem>)}
                                        </List>
                                    </div>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </main>
            </React.Fragment>
        );

    }
}

PowAlert.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(PowAlert);