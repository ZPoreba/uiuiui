import * as React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import { NavListItems } from './NavListItems';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Button from '@material-ui/core/Button';
import { styles } from './Styles/Styles';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { withRouter } from 'react-router-dom';
import { logout } from '../../Redux/Actions/Actions';
import { bindActionCreators } from 'redux';

class Layout extends React.Component {
    state = {
        open: false,
    }

    handleMenu = () => this.setState(currentState => ({
        open: !currentState.open
    }));

    logout = () => {
        this.props.logout();
        this.props.historu.push('/');
    }

    render() {
        const { isLoggIn, children, classes, history } = this.props;
        const { open } = this.state;

        return <div className={classes.root}>
            <CssBaseline />
            <AppBar position="absolute" className={classNames(classes.appBar, open && classes.appBarShift)}>
                <Toolbar disableGutters={!open} className={classes.toolbar}>
                    {isLoggIn && <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        onClick={this.handleMenu}
                        className={classNames(
                            classes.menuButton,
                            open && classes.menuButtonHidden,
                        )}
                    >
                        <MenuIcon />
                    </IconButton>
                    }
                    <Typography
                        component="h1"
                        variant="h6"
                        color="inherit"
                        noWrap
                        className={classes.title}
                        onClick={() => this.props.history.push('/')}
                    >
                        MySlope
                    </Typography>
                    {isLoggIn ? (<div className={classes.sectionDesktop}>
                        <div className={classes.search}>
                            <div className={classes.searchIcon}>
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Find slope..."
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                                onClick={() => history.push('/slopeSearch')}
                            />
                        </div>
                            <IconButton color="inherit">
                                <Badge badgeContent={17} color="secondary">
                                    <NotificationsIcon />
                                </Badge>
                            </IconButton>
                            <IconButton
                                aria-owns={'material-appbar'}
                                aria-haspopup="true"
                                color="inherit"
                            >
                                <AccountCircle onClick={() => history.push('/profile')} />
                            </IconButton>
                            <Button color="inherit" onClick={this.logout}>Logout</Button>
                        </div>
                    ) : (
                        <div>
                            <Button color="inherit" onClick={() => history.push('/login')}>Login</Button>
                            <Button color="inherit" onClick={() => history.push('/register')}>Register</Button>
                        </div>
                    )}
                </Toolbar>
            </AppBar>
            {isLoggIn && <Drawer variant="permanent"
                classes={{
                    paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
                }}
                open={open}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={this.handleMenu}>
                        <ChevronLeftIcon />
                    </IconButton>
                </div>
                <Divider />
                <List>
                    <NavListItems history={history}/>
                </List>
                <Divider />
            </Drawer>
            }
            <main className={classes.content}>
                <div className={classes.appBarSpacer} />
                {children}
            </main>
        </div>
    }
}

const isLoggedIn = state => ({
    isLoggIn: state.AuthenticationReducer.isLoggIn
});

const logoutActionMapper = dispatch => {
    return bindActionCreators({ logout }, dispatch);
};

export default connect(
    isLoggedIn,
    logoutActionMapper
)(
    withStyles(styles)(
        withRouter(Layout)
    )
);