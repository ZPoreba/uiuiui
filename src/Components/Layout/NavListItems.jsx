import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountCircle from '@material-ui/icons/AccountCircle';
import PeopleIcon from '@material-ui/icons/People';
import Message from '@material-ui/icons/Message';
import Settings from '@material-ui/icons/Settings';
import Cloud from '@material-ui/icons/Cloud';
import Badge from '@material-ui/core/Badge';

export const NavListItems = (props) => {
  const { history } = props;
  return <div>
    <ListItem button onClick={() => { history.push('/profile') }}>
      <ListItemIcon>
        <AccountCircle />
      </ListItemIcon>
      <ListItemText primary="Profile" />
    </ListItem>
    <ListItem button onClick={() => { history.push('/friends') }}>
      <ListItemIcon >
        <PeopleIcon />
      </ListItemIcon>
      <ListItemText primary="Friends" />
    </ListItem>
    <ListItem button onClick={() => { history.push('/powAlert') }}>
      <ListItemIcon>
        <Cloud />
      </ListItemIcon>
      <ListItemText primary="Pow alert" />
    </ListItem>
    <ListItem button onClick={() => { history.push('/messages') }}>
      <ListItemIcon>
        <Badge color="secondary" badgeContent={7} >
          <Message />
        </Badge>
      </ListItemIcon>
      <ListItemText primary="Messages" />
    </ListItem>
    <ListItem button onClick={() => { history.push('/settings') }}>
      <ListItemIcon>
        <Settings />
      </ListItemIcon>
      <ListItemText primary="Settings" />
    </ListItem>
  </div>
}
