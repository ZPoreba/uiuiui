import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CreateIcon from '@material-ui/icons/Create';
import SocialLogin from '../Common/SocialLogin';
import { styles } from './Styles/Styles';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { bindActionCreators } from 'redux';
import { register } from '../../Redux/Actions/Actions';

class Register extends React.Component {
    state = {
        password: '',
        passwordConfirmation: '',
        email: '',
        username: '',
        validation: {
            isPasswordValid: true,
            isUsernameValid: true,
            isEmailValid: true
        },
        tAcAccept: false,
        showMessage: false,
        message: ''
    }

    onInputChange = (event) => this.setState({
        [event.target.name]: event.target.value
    });

    onCheckChange = () => this.setState(currentState => ({ tAcAccept: !currentState.tAcAccept }));

    closeButton = (
        <Button color="secondary" size="small" onClick={() => this.setState({ showMessage: false })}>
            CLOSE
        </Button>
    );

    register = (event) => {
        event.preventDefault();  
        const emailRegex = new RegExp("^[^@]+@[^@]+.[^@]+$");
        const passwordRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        const { password, email, username, passwordConfirmation } = this.state;

        if(username.trim() === '') {
            this.setState({
                showMessage: true,
                message: 'Username can not be empty'
            });
        } else if(email.trim() === ''){
            this.setState({
                showMessage: true,
                message: 'Email can not be empty'
            });
        } else if(password.trim() === ''){
            this.setState({
                showMessage: true,
                message: 'Password can not be empty'
            });
        } else if(passwordConfirmation.trim() === ''){
            this.setState({
                showMessage: true,
                message: 'Password confiramtion can not be empty'
            });
        } else {
            this.setState(currentState => ({
                validation: {
                    isPasswordValid: passwordRegex.test(String(currentState.password)),
                    isEmailValid: emailRegex.test(String(currentState.email).toLowerCase()),               
                },
                showMessage: !passwordRegex.test(String(currentState.password)) || !emailRegex.test(String(currentState.email).toLowerCase()),
                message: `Invalid ${passwordRegex.test(String(currentState.password)) ? '' : 'password'} ${emailRegex.test(String(currentState.email).toLowerCase()) ? '' : 'email'}`
            }), () => {
                const { validation } = this.state;
    
                if(validation.isPasswordValid && validation.isEmailValid){
                    const { password, email, username, tAcAccept } = this.state;
                    const passwordMatch = this.state.password === this.state.passwordConfirmation;
                    if(passwordMatch && tAcAccept){    
                        this.props.register(email, password, username);              
                        this.props.history.push('/login');
                    } else {
                        this.setState({
                            showMessage: true,
                            message: (!passwordMatch && 'Passwords doesnt match') || (!tAcAccept && 'Accept terms and conditions')
                        });
                    }
                }
            });
        }
    }

    render() {
        const { classes, isLoggIn } = this.props;
        const { tAcAccept, showMessage, message, password, passwordConfirmation, email, username } = this.state;

        return isLoggIn ? (
            <Redirect to='/' />
        ) : (
                <main className={classes.main}>
                    <CssBaseline />
                    {showMessage && <SnackbarContent className={classes.snackbar} message={message} action={this.closeButton} />}
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <CreateIcon />
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign up
                    </Typography>
                        <form className={classes.form}>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Username</InputLabel>
                                <Input onChange={this.onInputChange} id="username" value={username} name="username" autoComplete="username" />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="email">Email Address</InputLabel>
                                <Input onChange={this.onInputChange} id="email" name="email" value={email} autoComplete="email" autoFocus />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Password</InputLabel>
                                <Input onChange={this.onInputChange} name="password" value={password} type="password" id="password" />
                            </FormControl>
                            <FormControl margin="normal" required fullWidth>
                                <InputLabel htmlFor="password">Confirm password</InputLabel>
                                <Input onChange={this.onInputChange} name="passwordConfirmation" value={passwordConfirmation} type="password" id="password" />
                            </FormControl>
                            <FormControlLabel
                                control={<Checkbox value="remember" color="primary" />} value={tAcAccept}
                                label="I accept term and conditions" onChange={this.onCheckChange}
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                onClick={this.register}
                            >
                                Sign up
                            </Button>
                            <SocialLogin />
                        </form>
                    </Paper>
                </main>
            )
    }
}

const isLoggedIn = state => ({
    isLoggIn: state.AuthenticationReducer.isLoggIn,
});

const registerActionMapper = dispatch => {
    return bindActionCreators({ register }, dispatch);
};

export default connect(
    isLoggedIn,
    registerActionMapper
    )(
    withStyles(styles)(Register)
);