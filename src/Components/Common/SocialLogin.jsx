import * as React from 'react';
import Button from '@material-ui/core/Button';
import withStyles from '@material-ui/core/styles/withStyles';

const style = theme => ({
    login: {
        marginBottom: theme.spacing.unit * 1.5
    }
})

const SocialLogin = (props) => (
    <div>
        <hr />
        <Button
            fullWidth
            variant="contained"
            color="secondary"
            className={props.classes.login}
        >
        Login with Facebook
        </Button>
        <Button
            fullWidth
            variant="contained"
            color="primary"
            className={props.classes.login}
        >
        Login with TWITTER
        </Button>
    </div>
)

export default withStyles(style)(SocialLogin);