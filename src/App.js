import React from 'react';
import { Route, Switch } from 'react-router';
import HomeContainer from './Components/HomePage/HomeContainer';
import Login from './Components/Login/Login';
import Register from './Components/Register/Register';
import NoPage from './Components/NoPage/NoPage';
import Friends from "./Components/Friends/Friends"
import Layout from './Components/Layout/Layout';
import Profile from "./Components/Profile/Profile"
import SlopeSearch from './Components/SlopeSearch/SlopeSearch';
import Slope from './Components/Slope/Slope';
import PowAlert from './Components/PowAlert/PowAlert';
import Messages from './Components/Messages/Messages';

const App = () => (
  <Switch> 
    <Route path='/login' component={Login} />
    <Route path='/register' component={Register} />    
    <Layout>
      <Route path='/messages' component={Messages} />
      <Route path='/slope' component={Slope} />
      <Route path='/slopeSearch' component={SlopeSearch} />
      <Route path='/friends' component={Friends} />
      <Route path='/profile/:id?' component={Profile} />
      <Route path='/powAlert' component={PowAlert} />
      <Route exact path='/' component={HomeContainer} />
    </Layout>
    <Route path='*' component={NoPage} />
  </Switch>
)

export default App;
