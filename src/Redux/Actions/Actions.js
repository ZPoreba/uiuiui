import { LOG_IN, LOG_OUT, REGISTER } from './ActionsTypes';

export const login = () => {
    return (dispatch) => {
        dispatch({ 
            type: LOG_IN 
        })
    }
}

export const logout = () => {
    return (dispatch) => {
        dispatch({ 
            type: LOG_OUT 
        })
    }
}

export const register = (email, password, username) => {
    return (dispatch) => {
        dispatch({
            type: REGISTER,
            email: email,
            password: password,
            username: username
        })
    }
}