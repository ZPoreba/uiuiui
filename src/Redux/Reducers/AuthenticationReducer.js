import { CHECK_PASSWORD, LOG_IN, LOG_OUT, REGISTER } from '../Actions/ActionsTypes';

const initialState = {
    isLoggIn: false,
    email: 'example123@gmail.com',
    password: 'Password.',
    username: ''
}

const AuthenticationReducer = (state = initialState, action) => {
    switch(action.type){
        case CHECK_PASSWORD: {
            return {
                ...state,
                username: state.username,
                password: state.password
            }
        }
        
        case LOG_IN: {
            return {
                ...state,
                isLoggIn: true
            }
        }

        case LOG_OUT: {
            return {
                ...state,
                isLoggIn: false
            }
        }
        
        case REGISTER: {
            return {
                ...state,
                email: action.email,
                password: action.password,
                username: action.username
            }
        }

        default: 
            return state;
    }
}

export default AuthenticationReducer;