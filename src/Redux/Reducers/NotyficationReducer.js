import { PUSH_NOTYFICATION, RECIVE_MESSAGES } from '../Actions/ActionsTypes';

const initialState = {
    messsages: [],
    notyfication: []
}

const NotyficationReducer = (state = initialState, action) => {
    switch(action.type){
        case PUSH_NOTYFICATION: {
            return {
                ...state,
                messsages: [...state.messsages, action.messsages]
            }
        }
        
        case RECIVE_MESSAGES: {
            return {
                ...state,
                notyfication: [...state.notyfication, action.notyfication]
            }
        }

        default: 
            return state;
    }
}

export default NotyficationReducer;