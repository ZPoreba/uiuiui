import { combineReducers } from "redux";
import AuthenticationReducer from './AuthenticationReducer';
import NotyficationReducer from './NotyficationReducer';

const rootReducer = combineReducers({ AuthenticationReducer, NotyficationReducer });

export default rootReducer;